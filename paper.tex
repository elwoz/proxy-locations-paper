% Must be done first of all.
\RequirePackage[l2tabu,orthodox]{nag}
% Consolidate options set by pdfx/acmart/me.
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage
    {pdftex,pdfa,bookmarksnumbered,unicode,hyperfootnotes=false}{hyperref}
\PassOptionsToPackage{rgb,hyperref,prologue}{xcolor}
% These packages need to be loaded before the document class has a
% chance to load hyperref.
% Need to use PDF-A version 2 because several graphics contain intentional
% use of alpha transparency.  'u' means all fonts are Unicode mapped.
\RequirePackage[a-2u]{pdfx}
%
\documentclass[sigconf,authorversion=true]{acmart}

\usepackage[utf8]{inputenc}
% inputenc doesn't set the space factor of Unicode close curly quote
% correctly.  (In current versions, the \sfcode of ’ is 0, but the
% \sfcode of ” is still 1000.  Both should be zero.)
\AtBeginDocument{%
  \sfcode\csname\encodingdefault\string\textquotedblright\endcsname=0%
  \sfcode\csname\encodingdefault\string\textquoteright\endcsname=0%
}
\DeclareUnicodeCharacter{00A2}{\textcent} % ¢

\usepackage{bookmark}
\usepackage{pifont}

\usepackage[per-mode=symbol,detect-weight=true]{siunitx}
\DeclareSIUnit{\clight}{\text{\ensuremath{c}}}
\newcommand{\kmms}[1]{\SI{#1}{\km\per\ms}}

% acmart uses natbib for citation handling.
% compressed citation lists look silly in the one place they trigger.
\setcitestyle{nocompress}
% shim to the less cryptic biblatex commands used in the body.
\let\Textcite=\Citet
\let\textcite=\citet
\let\cite=\citep

% Miscellaneous settings
\overfullrule=5pt
\pdfsuppresswarningpagegroup=1
\hyphenation{
  ano-nym-ity brows-er brows-ers Ja-va-Script Phill-ipa pro-vi-der pro-vi-ders
}

\renewcommand{\paragraph}[1]{\par\medskip\textbf{#1.}}

% todo's
\newcommand{\zackw}[1]{{\color{green}[ZW: #1]}}
\newcommand{\Pnote}[1]{{\color{magenta}[PG: #1]}}
\newcommand{\nicolasc}{\textcolor{blue}{NC:} \ding{110}\ding{43}\textcolor{blue}}

%% For reference when sizing figures:
% Text block height: \textheight  = 626.0pt     = 8.662in
% Text block width:  \textwidth   = 506.295pt   = 7.005in
% Column width:      \columnwidth = 241.14749pt = 3.337in
%
% (n.b. a TeX point is 1/72.27 inch; PS, PDF, CSS, SVG, etc use 1/72
% inch instead, see https://en.wikipedia.org/wiki/Point_(typography))

\begin{document}

\title{How to Catch when Proxies Lie}
\subtitle{Verifying the Physical Locations of Network Proxies with
   Active Geolocation}

\author{Zachary Weinberg}
\affiliation{\institution{Carnegie Mellon University}}
\email{zackw@cmu.edu}
\author{Shinyoung Cho}
\affiliation{\institution{Stony Brook University}}
\email{shicho@cs.stonybrook.edu}
\author{Nicolas Christin}
\affiliation{\institution{Carnegie Mellon University}}
\email{nicolasc@cmu.edu}
\author{Vyas Sekar}
\affiliation{\institution{Carnegie Mellon University}}
\email{vsekar@cmu.edu}
\author{Phillipa Gill}
\affiliation{\institution{University of Massachusetts}}
\email{phillipa@cs.umass.edu}

\copyrightyear{2018}
\acmYear{2018}
\setcopyright{acmlicensed}
\acmConference[IMC '18]
  {2018 Internet Measurement Conference}
  {October 31--November 2, 2018}
  {Boston, MA, USA}
\acmBooktitle{2018 Internet Measurement Conference (IMC '18),
  October 31--November 2, 2018, Boston, MA, USA}
\acmPrice{15.00}
\acmDOI{10.1145/3278532.3278551}
\acmISBN{978-1-4503-5619-0/18/10}

\begin{CCSXML}
<ccs2012>
<concept>
<concept_id>10003033.10003079.10011704</concept_id>
<concept_desc>Networks~Network measurement</concept_desc>
<concept_significance>500</concept_significance>
</concept>
<concept>
<concept_id>10003033.10003083.10003090</concept_id>
<concept_desc>Networks~Network structure</concept_desc>
<concept_significance>300</concept_significance>
</concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[500]{Networks~Network measurement}
\ccsdesc[300]{Networks~Network structure}

\keywords{active geolocation, virtual private networks,
  network proxies}

% acmart.cls requires the abstract environment to come before \maketitle
\begin{abstract}
Internet users worldwide rely on commercial network proxies both to
conceal their true location and identity, and to control their
apparent location.  Their reasons range from mundane to
security-critical.  Proxy operators offer no proof that their
advertised server locations are accurate.  IP-to-location databases
tend to agree with the advertised locations, but there have been many
reports of serious errors in such databases.

In this study we estimate the locations of 2269 proxy servers from
ping-time measurements to hosts in known locations, combined with AS
and network information.  These servers are operated by seven proxy
services, and, according to the operators, spread over 222 countries
and territories.  Our measurements show that one-third of them are
definitely not located in the advertised countries, and another third
might not be.  Instead, they are concentrated in countries where
server hosting is cheap and reliable (e.g.\ Czech Republic, Germany,
Netherlands, UK, USA).

In the process, we address a number of technical challenges with
applying active geolocation to proxy servers, which may not be
directly pingable, and may restrict the types of packets that can be
sent through them, e.g.\ forbidding \texttt{traceroute}. We also test
three geolocation algorithms from previous literature, plus two
variations of our own design, at the scale of the whole world.
\end{abstract}

\maketitle
\renewcommand{\shortauthors}{Zachary Weinberg et al.}

\input introduction.tex
\input background.tex
\input algorithms.tex
\input measurement.tex
\input algo-testing.tex
\input proxy-locations.tex
\input relwork.tex
\input discussion.tex
\input ack.tex
\bibliographystyle{bib/ACM-Reference-Format}
\bibliography{bib/geolocation,bib/anonymity,bib/censorship,bib/sidechan,bib/misc}

\appendix
% fix duplicate PDF destination:
\renewcommand\theHsection{\Alph{section}}

\input continents.tex
\end{document}
