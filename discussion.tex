\section{Discussion}\label{s:discussion}

We have demonstrated the viability of a simple algorithm for active
geolocation, CBG++, at global scale, especially when it is possible to
use a crude location estimate to select landmarks within the same
continent as the target.  We have also confirmed that it is possible
to geolocate proxy servers, even when they cannot be directly pinged.
Our implementation of the four geolocation algorithms, as well as our
measurement code, is publicly available at
\url{https://github.com/zackw/active-geolocator}.

We have also put to the test the location claims of seven major
commercial proxy operators. Our findings are dire: advertised server
locations cannot be relied upon, especially when the operators claim
to have servers in locations where server hosting is difficult.  At
most 70\% of the servers are where their operators say they are, and
that is giving them the full benefit of the doubt; we can only
confidently confirm the providers' claims for about 50\% of the
servers, and all of those are in countries where hosting is easy.
Provider~A is especially misleading, but all seven of the providers
we evaluated had at least a couple of questionable hosts.  We shared
our results with the providers and asked for an explanation, but all
of them declined to respond.

Our results call into question the validity of any network measurement
that used VPNs to gain location diversity, especially to diversify
beyond Europe and North America.  Also, despite a steady stream of
reports that IP-to-location databases are unreliable
(e.g.~\cite{shavitt.2011.databases, poese.2011.databases,
  gharaibeh.2017.router}) they are still relied upon in numerous
contexts; we add our voices to those earlier notes of caution.

As we mentioned in the introduction, many of a VPN provider's
customers might be content to appear to be in a specific country.  We
are not aware of anyone having investigated what VPN customers think
they are buying, when they subscribe to a provider that advertises
servers in many countries.  It would be interesting to find out.
Relatedly, while it is well-known that commercial IP-to-location
databases contain \emph{errors}, we are not aware of anyone having
investigated the possibility of their containing deliberately false
information (perhaps because the database compilers themselves were
deceived).

One might also wonder whether the VPN operators could actively mislead
investigators about the true location of their servers, by interfering
with round-trip time measurements.  They have no particular reason to
do this now, but if active location validation becomes common, they
might be motivated to try it.  Previous work has found that hostile
geolocation targets can indeed foul a position estimate.
\Textcite{gill.2010.dude} and
others~\cite{muir.2009.evasion,abdou.2015.cpv} report that
\emph{selective} added delay can displace the predicted region, so
that its centroid is nowhere near the target's true location; more
sophisticated delay-distance models are more susceptible to this,
especially if they derive minimum as well as maximum feasible
distances from delay measurements.  \Textcite{abdou.2017.manipulation}
go further, describing two methods for modifying ICMP echo replies so
that some landmarks compute \emph{smaller} round-trip times than they
should; with this ability, an adversarial target can shift the
predicted region to be anywhere in the world, irrespective of its true
location.

Our measurements use TCP handshakes, which include anti-forgery
measures, rather than ICMP echo exchanges; also, we can trust both the
landmarks and the host running the measurement tool.  It is the VPN
proxy, in the middle, that is the target of geolocation and not
trusted.  Unfortunately, being in the middle means it is \emph{easier}
for a proxy target to manipulate RTTs both up and down, than it was
for an end-host target as considered by \citeauthor{abdou.2017.manipulation}
% previous sentence gets its period from the \citeauthor
It can selectively delay packets, and it can also selectively forge
early SYN-ACKs without needing to guess sequence numbers, since it
sees the SYNs.  Conceivably, we could prevent this by using landmarks
that report their own idea of the time, unforgeably,
e.g.\ authenticated NTP servers~\cite{dowling.2016.ants}---if we could
be sure that our measurement client and all of the landmarks already
had synchronized clocks, which is a substantial engineering challenge
in itself.

Finally, our Web-based measurement technique could be used to
geolocate any visitor to a malicious website without their knowledge
or consent.  This would be foiled by the use of a proxy, VPN, Tor, or
similar, in much the same way that IP-based geolocation is foiled by
masking one's IP address with these tools.  However, it is still an
argument against allowing Web applications to record high-precision
information about page load timings, and we plan to discuss this with
the major browser vendors.

\subsection{Future work}\label{s:future-work}

We were only able to include seven VPN providers in this study; there
are at least 150 others, some of which make claims nearly as
extravagant as provider~A.  We intend to expand the study to cover
as many additional providers as possible, in cooperation with
researchers and consumer watchdog organizations looking into other
ways commercial VPN providers may fail to live up to their users'
expectations.  This will also allow us to repeat the measurements over
time, and report on whether providers become more or less honest as
the wider ecosystem changes.

In order to understand the errors added to our position estimates by
the indirect measurement procedure described in
Section~\ref{s:proxy-adaptations}, we are planning to set up
test-bench VPN servers of our own, in known locations worldwide, and
attempt to measure their locations both directly and indirectly.

While our two-phase measurement process is fast and efficient, it also
produces noisy groups of measurements like those shown in
Figure~\ref{f:disambig-metadata}.  We think this can be addressed
with an iterative refinement process, in which additional probes and
anchors are included in the measurement as necessary to reduce the
size of the predicted region.

We are experimenting with an additional technique for detecting
proxies in the same data center, in which we measure round-trip times
to each proxy from each other proxy.  Pilot tests indicate that some
groups of proxies (including proxies claimed to be in separate
countries) show less than \SI{5}{\ms} round-trip times among
themselves, which practically guarantees they are on the same local
network.

It would be valuable to have a measurement tool that is as
user-friendly as the existing Web-based tool, but as accurate as the
command-line tool.  The Web-based tool could reliably record the time
for a single round-trip, and perhaps also avoid some of the
Windows-specific overhead and noise, if it could use the W3C
Navigation Timing API~\cite{wang.2012.nav-timing}.  This API gives Web
applications access to detailed information about the time taken for
each stage of an HTTP query-response pair.  Unfortunately, it can only
be used if each server involved allows it, and currently none of the
RIPE Atlas anchors and probes do.  We plan to discuss the possibility
with the RIPE team.  Of course, as we mentioned above, the fact that
active geolocation from a Web application is possible at all arguably
constitutes a privacy leak in Web browsers, and we also plan to
discuss that with the browser vendors.

RIPE Atlas anchors tend to be on subnetworks with more stable, less
congested connectivity to the global backbone than is typical for
their locale.  That could mean each anchor's CBG++ bestline,
calibrated from measurements of round-trip times to the other anchors,
overestimates the distance packets can typically travel from that
anchor.  Overestimation leads only to greater uncertainty in predicted
locations, whereas underestimation leads to failure (as discussed in
Section~\ref{s:cbgplusplus}).  Still, this is a source of error that
should be quantified.  We are considering adding other measurement
constellations, such as the CAIDA Archipelago~\cite{caida.nd.ark} and
PlanetLab~\cite{planetlab.nd}, to our landmark set.  This would allow
us to compare the delay-distance relationships observed across
constellations to those observed within a single constellation, and
thus investigate the degree of overestimation.  Additional
constellations would also improve our landmark coverage outside Europe
and North America.  All of the above are also concentrated in the
“developed world,” but in sparse networks, each new landmark helps a
great deal~\cite{eriksson.2013.geometry}.
