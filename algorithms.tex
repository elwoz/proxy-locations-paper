\begin{figure*}[t!]
\centering
\includegraphics{figure/cal-example}
\caption{Example calibration scatterplots for CBG, (Quasi-)Octant, and
  Spotter.}
\label{f:example-calibration}
\end{figure*}

\section{Algorithm Selection}\label{s:algorithm-selection}

Since the proxies we are investigating could be spread all over the
world, we must find an active geolocation algorithm that will work at
the scale of the whole world.  We reimplemented four active
geolocation algorithms from earlier papers: CBG~\cite{gueye.2004.cbg},
Octant~\cite{wong.2007.octant}, Spotter~\cite{laki.2011.spotter}, and
an Octant/Spotter hybrid of our own invention.  We did not have access
to the original implementations, and we had to fill in gaps in all
their published descriptions.  All the software we developed for this
project is open-source and available
online.\footnote{\url{https://github.com/zackw/active-geolocator}}

\Textcite{eriksson.2010.learning} recommend considering external facts
about where a server could plausibly be, such as “on land, and not in
Antarctica.”  We take this advice and exclude all terrain north of
$\ang{85}\,\text{N}$ and south of $\ang{60}\,\text{S}$ from the final
prediction region for each target and algorithm.  Using the 2012
Natural Earth~\cite{patterson.2012.ne} map of the world, we also
exclude oceans and lakes.  We do not, however, exclude any islands, no
matter how small or remote, because some of the proxy providers do
claim to have servers on remote islands (e.g. Pitcairn).

\subsection{Constraint-Based Geolocation}\label{s:cbg}

Constraint-Based Geolocation (CBG) is one of the oldest and simplest
multilateration algorithms.  It uses a linear model for the
delay-distance relationship, limited by a “baseline” speed of
\kmms{200}, or $\frac{2}{3}c$, which is approximately how fast signals
propagate in fiber-optic cable.  For each landmark, CBG computes a
“bestline” from the calibration data, which is as close as possible to
all of the data points on a scatterplot of delay as a function of
distance, while remaining below all of them, and above the baseline.
This will be a speed \emph{slower} than \kmms{200}, and will therefore
give a \emph{smaller} estimate of how far a packet could have gone in
a given time.  Each landmark's bestline gives the maximum distance for
a round-trip measurement to that landmark.

The left panel of Figure~\ref{f:example-calibration} shows an example
calibration for CBG. The blue dots are round-trip time measurements
taken by one RIPE anchor.  The bestline (solid) is above the baseline
(dotted); it passes through exactly two data points, with all the
others above.  It corresponds to a speed of \kmms{93.5}---less than
half the theoretical maximum.  The “slowline” will be explained in
Section~\ref{s:cbgplusplus}.

\subsection{Quasi-Octant}\label{s:q-octant}

Octant elaborates on CBG in two ways.  First, it estimates both the
maximum and the minimum distance to each landmark, and draws rings on
the map, not disks.  Second, Octant uses piecewise-linear curves for
both distance models.  These are defined by the convex hull of the
scatterplot of delay as a function of distance, up to 50\% and 75\% of
all round-trip times, respectively.  Observations beyond those cutoffs
are considered unreliable, so Octant uses fixed empirical speed
estimates for longer round-trip times. The middle panel of
Figure~\ref{f:example-calibration} shows an example Octant
calibration, with the same data as the CBG calibration to its left.
The convex hull is drawn with solid lines and the fixed empirical
speeds with dashed lines.

Octant includes features that depend on route traces, such as a
“height” factor to eliminate the effect of a slow first hop from any
given landmark.  Since we cannot collect route traces (see
Section~\ref{s:meas-tool-web}), these have been omitted from our
re-implementation, and we call it “Quasi-Octant” to denote that
change.

\subsection{Spotter}\label{s:spotter}

Spotter~\cite{laki.2011.spotter} uses an even more elaborate
delay-distance model.  It computes the mean and standard deviation of
landmark-landmark distance as a function of delay, and fits “a
polynomial” to both.  Unlike CBG and Octant, a single fit is used for
all landmarks.  The paper does not specify the degree of the
polynomial, or the curve-fitting procedure; we use cubic polynomials,
fit by least squares, and constrain each curve to be increasing
everywhere (anything more flexible led to severe overfitting in pilot
tests).

Spotter also uses a probabilistic multilateration method.  It
estimates the distance from each landmark to the target as a Gaussian
distribution, with mean $\mu$ and standard deviation $\sigma$ given by
the fitted curves. This produces a ring-shaped probability
distribution over the surface of the Earth; the rings for each
landmark are combined using Bayes' Rule to form the final prediction
region.

The right panel of Figure~\ref{f:example-calibration} shows an example
Spotter calibration.  The solid line is the best cubic fit for the
mean $\mu$ of the distance-delay relationship; dashed, dash-dot, and
dotted lines are drawn at $\mu \pm \sigma$, $\mu \pm 3\sigma$, and
$\mu \pm 5\sigma$ respectively.

\subsection{Quasi-Octant/Spotter Hybrid}\label{s:hybrid}

To separate the effect of Spotter's probabilistic multilateration from
the effect of its cubic-polynomial delay model, we also implemented a
hybrid that uses Spotter's delay model, but Quasi-Octant's ring-based
multilateration.  The minimum and maximum radii of the ring are set to
$\mu - 5\sigma$ and $\mu + 5\sigma$, respectively.
