\section{Algorithm Testing}\label{s:algorithm-testing}

In order to test our geolocation algorithms on hosts they hadn't been
calibrated with, we crowdsourced a second set of hosts in known
locations.\footnote{This study was approved by our university's IRB.}
40 volunteers, recruited from a variety of mailing lists and online
forums, and another 150 paid contributors, recruited via Mechanical
Turk for 25¢ each, provided us with the approximate physical location
of their computers (rounded to two decimal places in latitude and
longitude, or roughly \SI{10}{\km} of position uncertainty) and a set
of round-trip times to RIPE Atlas anchors and probes, using the
Web-based measurement tool described in
Section~\ref{s:meas-tool-web}.  Their self-reported locations are
shown in Figure~\ref{f:validation-map}. Like the RIPE anchors, the
majority are in Europe and North America, but we have enough
contributors elsewhere for statistics.

Our priority was to find an algorithm that would always include the
true location of each host in its predicted region, even if this meant
the region was fairly imprecise.  To put it another way, when
investigating the locations of commercial proxies, we want to be
certain that the proxy is where we say it is, even if that means we
cannot assure that it is not where the provider says it is.

In figure~\ref{f:prediction-precision}, panel~A, we plot an
empirical CDF of how far outside the predicted region each true
location is, for each of the four algorithms.  This is a direct
measure of each algorithm's failure to live up to the above
requirement.  None of the algorithms are perfect, but CBG does better
than the other three, producing predictions that do include the true
location for 90\% of the test hosts, and are off by less than
\SI{5000}{\km} for 97\% of them.  Hybrid and Quasi-Octant's
predictions miss the mark for roughly 50\% of the test hosts, but they
are off by less than \SI{5000}{\km} for roughly 90\%.  Fully half of
Spotter's predictions are off by more than \SI{10000}{\km}.

In panels~B and~C of Figure~\ref{f:prediction-precision}, we look
into \emph{why} the predictions miss the true region.  Panel~B shows
that the distances from the \emph{centroid} of each algorithm's
predictions, to the true locations, are about the same for all four
algorithms, and panel~C shows that CBG produces predictions that are
much larger than the other three.  We conclude that none of the
algorithms can reliably center their predicted region on the true
location, but CBG's predictions are usually big enough to cover the
true location anyway, whereas the other three algorithms' predictions
are not big enough.

Why should CBG be so much more effective?  Looking again at the
calibration data in Figure~\ref{f:example-calibration}, we observe that
most of the data points are well above CBG's bestline.  Quasi-Octant
and Spotter draw more information from these points than CBG does.  If
most of those points are dominated by queueing and circuitousness
delays, rather than the great-circle distance between pairs of
landmarks, that would lead Quasi-Octant and especially Spotter to
underestimate the speed packets can travel, therefore predicting
regions that are too small.  Large queueing delays also invalidate the
assumption, shared by both Quasi-Octant and Spotter, that there is a
\emph{minimum} speed packets can travel~\cite{li.2013.modconn}.

Most of our crowdsourced contributors used the web application under
Windows.  As we described in Section~\ref{s:tool-validation}, this
introduces extra noise and “high outliers” into the measurements.  CBG
has an inherent advantage in dealing with measurements biased upward,
since it always discards all but the quickest observation for each
landmark, its bestlines are the fastest travel time consistent with
the data, and it does not assume any minimum travel speed when
multilaterating.  Crowdsourced measurements using only the
command-line tool might have allowed Quasi-Octant and Spotter to do
better.  However, measurements taken through proxies are liable to
suffer extra noise and queuing delays as well.  We could thus argue
that the web application's limitations make the crowdsourced test a
better simulation of the challenges faced by active geolocation of
proxies.

\subsection{Eliminating Underestimation: CBG++}\label{s:cbgplusplus}

\begin{figure}[t!]
\centering
\includegraphics{figure/underestimation}
  \caption{CBG bestline and baseline estimates compared to the true
    distance.}
  \label{f:underestimation}
\end{figure}

\begin{figure}
\centering
\includegraphics{figure/distance-effectiveness}
\caption{Proportion of measurements that had an effect on the final
  prediction region, as a function of distance between landmark and
  target; for effective measurements, the amount by which they reduced
  the size of the final region.  The total land area
  of Earth is roughly 150 square megameters (\si{\mega\meter^2}), and
  the land area of Egypt is roughly \SI{1}{\mega\meter^2}.}%
\label{f:lm-effectiveness}
\end{figure}


Regardless of the reasons, CBG clearly is the most effective algorithm
in our testing, but it still doesn't always cover the true location
with its predictions.  We made two modifications in order to eliminate
this flaw, producing a new algorithm we call CBG++.

CBG's disks can only fail to cover the true location of the target if
some of them are too small.  A disk being too small means the
corresponding bestline underestimates the distance that packets could
travel.  This can easily happen, for instance, when the network near a
landmark was congested during calibration~\cite{komosny.2015.underestimate}.
Not only can an underestimate make the prediction miss the target, it
can make the intersection of all the disks be empty, meaning that the
algorithm fails to predict \emph{any} location for the target.

To reduce the incidence of underestimation, we first introduced
another physical plausibility constraint.  CBG's bestlines are
constrained to make travel-speed estimates no faster than \kmms{200}
as packets can travel no faster than this in undersea cables.  We also
constrain them to make travel-speed estimates no \emph{slower} than
\kmms{84.5}; this is the “slowline” in the CBG panel of
Figure~\ref{f:example-calibration}.  The logic behind this number is:
No landmark can be farther than half the equatorial circumference of
the Earth, \SI{20037.508}{\km}, from the target.  One-way travel times
greater than \SI{237}{\ms} could have involved a geostationary
communications satellite, and one such hop can bridge any two points
on the same hemisphere, so they provide no useful information.
$\SI{20037.508}{\km}/\SI{237}{\ms} = \kmms{84.5}$.

The slowline constraint is not enough by itself.
Figure~\ref{f:underestimation} shows the distribution of ratios of
bestline and baseline distance estimates to the true distances, for
all pairs of landmarks, with the slowline constraint applied.  We use
the landmarks themselves for this analysis, rather than the
crowdsourced test hosts, because we know their positions more
precisely and the ping-time measurements they make themselves are also
more accurate.  A small fraction of all bestline estimates are still
too short, and for very short distances this can happen for baseline
estimates as well.

We weed out the remaining underestimates with a more sophisticated
multilateration process.  For each landmark, we compute both the
bestline disk, and a larger disk using the baseline.  We find the
largest subset of all the baseline disks whose intersection is
nonempty; this is called the “baseline region.”  Any bestline disk
that does not overlap this region is discarded.  Finally we find the
largest subset of the remaining bestline disks whose intersection is
nonempty; this is the “bestline region.”  These subsets can be found
efficiently by depth-first search on the powerset of the disks,
organized into a suffix tree.  Retesting on the crowdsourced test
hosts, we found that this algorithm eliminated all of the remaining
cases where the predicted region did not cover the true location.

\subsection{Effectiveness of Landmarks}\label{s:lm-effectiveness}

To check the observations of \textcite{khan.2016.adaptive} and others,
that landmarks closer to the target are more useful, we measured the
round-trip time between all 250 RIPE Atlas anchors and the target for
all of the crowdsourced test hosts.  A large majority of all
measurements lead to disks that radically overestimate the possible
distance between landmark and target.  Multilateration produces the
same final prediction region even if these overestimates are
discarded.  We call these measurements \emph{ineffective}.  As shown
in Figure~\ref{f:lm-effectiveness}, effective measurements are more
likely to come from landmarks close to the target, but among the
effective measurements, there is no correlation between distance and
the amount by which the measurement reduced the size of the final
prediction.  This is because a distant landmark may still have only a
small overlap with the final prediction region, if it is distant in
just the right direction.

\begin{figure}
  \centering
  \includegraphics[width=0.5\hsize]{figure/rtt-through-proxy}
  \caption[Deriving RTT from a proxy from RTT through a proxy]
    {The RTT from a proxy to a landmark, A, must be derived from the
      RTT through a proxy to a landmark, B, and the RTT through a
      proxy back to the client, C: $A = B - \eta C$.}%
  \label{f:derived-rtt}%
\end{figure}
\begin{figure}
  \includegraphics{figure/direct-indirect-pings}%
  \caption[Relationship between direct and indirect round-trip times]
    {The relationship between direct and indirect round-trip times,
      $\eta$, is almost exactly 1/2.}%
  \label{f:direct-indirect}%
\end{figure}

\subsection{Adaptations for Proxies}\label{s:proxy-adaptations}

When taking measurements through a network proxy, each measured
round-trip time is the sum of the RTT from the client to the proxy,
and the RTT from the proxy to the landmark.  To locate the proxy, we
need to measure and subtract the RTT from the client to the proxy.  We
cannot measure this directly, because the proxy services usually
configure their hosts not to respond to ICMP ping packets, and
aggressively rate-limit incoming TCP connections.

Instead, we take inspiration from
\textcite{castelluccia.2009.fastflux} and have the client ping
\emph{itself}, through the VPN, as illustrated in
Figure~\ref{f:derived-rtt}.  This should take slightly more than
twice as long as a direct ping. Figure~\ref{f:direct-indirect}
shows the relationship between direct and indirect pings for all of
the proxies in the study that can be pinged both ways.  The blue line
is a robust linear regression, whose slope $\eta$ is the inverse of
the \texttt{RTT\_factor} described by
\citeauthor{castelluccia.2009.fastflux}.  In our case, the slope is
$0.49$ with $R^2 > 0.99$.
