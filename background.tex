\section{Background}\label{s:background}

\begin{figure}
  \centering
  \includegraphics[width=0.5\hsize]{figure/multilateration}
\caption[The principle of multilateration]
  {The principle of multilateration.  If something is within
  \SI{500}{\km} of Bourges,
  \SI{500}{\km} of Cromer, and
  \SI{800}{\km} of Randers,
  then it is in Belgium (roughly).}\label{f:multilateration}
\end{figure}

Existing methods for finding the physical locations of Internet hosts can
be divided into two general classes.  Passive methods collect location
information from regional Internet registries, location information
encoded in router hostnames, and private consultation with individual
ISPs~\cite{chandra.2015.alidade}, and produce a database mapping IP
addresses to locations.  These databases are widely used, but
notorious for their errors~\cite{shavitt.2011.databases,
  poese.2011.databases, gharaibeh.2017.router}, some of which are
significant enough that they make the news~\cite{hill.2016.kansas}.

Active methods, on the other hand, rely on measurements of packet
round-trip time between a \emph{target} host, which is to be located,
and a number of \emph{landmark} hosts, which are in known locations.
The simplest active method is to guess that the target is in the same
place as the landmark with the shortest round-trip
time~\cite{padman.2001.geog,ziviani.2005.accuracy,chen.2016.landmark}.
This breaks down when the target is not near any of the landmarks.
The next step up in complexity is to estimate, for each landmark, the
maximum distance that a packet could have traveled in the time
measured, and draw disks on a map, bounded by these distances.  The
target must be in the region where the disks all intersect.  This
process is called \emph{multilateration}.
Figure~\ref{f:multilateration} shows an example: measurements taken
from Bourges in France, Cromer in the UK, and Randers in Denmark
produce an intersection region roughly covering Belgium.

The central problem for network multilateration is that network packets do
not travel in straight lines.  Cables are laid on practical paths, not
great circles.  Network routes are optimized for bandwidth rather than
latency, leading to “circuitous” detours that can add thousands of
kilometers to the distance traveled~\cite{matray.2012.spatial,
  landa.2014.geography, krishnan.2009.cdn}. Intermediate routers can
add unbounded delays~\cite{li.2013.modconn}.  Distance and delay do
still correlate, but not in a straightforward way.  Much research on
active methods focuses on increasingly sophisticated models of the
delay-distance relationship~\cite{padman.2001.geog, gueye.2004.cbg,
  wong.2007.octant, arif.2010.geoweight, laki.2011.spotter,
  dong.2012.modeling, eriksson.2012.posit, matray.2012.spatial,
  landa.2014.geography}.  One common refinement is to assume a minimum
travel distance for any given delay, as well as a maximum.

\paragraph{Challenges of global geolocation}
When both landmarks and targets are in the same subcontinental region,
sophisticated models improve accuracy---if that region is Europe or
the USA.  On the other hand, for China, several papers report that
\emph{simple} models are more accurate~\cite{li.2013.modconn,
  ding.2015.ip, chen.2016.landmark}.  They propose that simple models
are more robust in the face of severe congestion.
\Textcite{li.2013.modconn} specifically points out that a minimum
travel distance assumption is invalid in the face of large queueing
delays at intermediate routers.  A second possibility is that
sophisticated models are more reliable when there are more possible
paths between landmarks and targets, as is the case in Europe and
North America, but not China~\cite{eriksson.2013.geometry}.  A third
is that models tested on PlanetLab nodes~\cite{planetlab.nd} gain an
unfair advantage due to the generally better connectivity enjoyed by
academic networks.  In Section~\ref{s:algorithm-testing}, we test four
algorithms, covering a range of model complexity, on hosts
crowdsourced from all over the world.  We also find that simple models
are more effective, overall, and our data is more consistent with the
congestion explanation.

Increasing the number of landmarks improves accuracy but also slows
down the measurement process, since all of the landmarks must send
packets to the target and wait for replies (or vice versa).  If they
all do this simultaneously, they may create enough extra network
congestion to invalidate the
measurement~\cite{holterbach.2015.ripe-interference}. Several
researchers have observed that landmarks far away from the target are
less useful, and proposed a two-stage process, in which a small number
of widely dispersed landmarks identify the subcontinental region where
the target lies, and then a larger group of landmarks within that
region pin down the target's position more
accurately~\cite{ziviani.2005.accuracy, khan.2016.adaptive,
  hu.2012.millions, ding.2015.ip}.

\paragraph{Challenges of geolocating proxies}
Less than ten percent of the proxies we are interested in testing will
respond to pings, and we do not have the ability to run measurement
programs on the proxies themselves.  We can only send packets
\emph{through} the proxies, which means the apparent round-trip time
to each landmark is the sum of the round-trip time from the proxy to
the landmark, and the round-trip time from our measurement client to
the proxy.  This is similar to the problem faced by
\textcite{castelluccia.2009.fastflux} when attempting to geolocate
botnet command-and-control servers, and we adopt the same solution, as
discussed further in Section~\ref{s:proxy-adaptations}.
