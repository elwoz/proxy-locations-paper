\section{Measurement Method}\label{s:measurement-method}

\begin{figure*}
  \centering
  \includegraphics{figure/landmarks-phase-2}
  \caption[Locations of RIPE Atlas anchors and stable probes]
          {Locations of the RIPE Atlas anchors (left) and
            probes with stable IPv4 addresses, as of April 2018.}
  \label{f:landmarks-map}
\end{figure*}

For all our experiments, we used the “anchor” hosts of RIPE
Atlas~\cite{ripe.2015.atlas} as landmarks.  RIPE Atlas is a worldwide
constellation of hosts dedicated to Internet measurement, composed of
“probes” and “anchors;” there are fewer anchors, but they are more
convenient for use as landmarks.  They are reliably available 24/7,
their documented locations are accurate, and they all continuously
ping each other and upload the round-trip times (RTT) to a publicly
accessible database.  At the time we began our experiments (July
2016), there were 207 usable anchors; during the course of
the experiment, 12 were decommissioned and another 61 were added.
Figure~\ref{f:landmarks-map} (left side) shows all the anchors'
locations.  The majority are in Europe; North America is also
well-represented.  While there are fewer anchors in Asia and South
America, and only a few in Africa, their geographic distribution is
adequate---the most difficult case for active geolocation is when all
of the landmarks are far away from the target, in the same
direction~\cite{matray.2012.spatial, eriksson.2013.geometry}.

\subsection{Two-phase measurement}\label{s:two-phase-measurement}
It takes several minutes to ping all 250 of the anchors.  Landmarks
far from the target do not contribute much useful information, as
we will discuss further in Section~\ref{s:lm-effectiveness}.  We
speed up the process with a two-phase measurement, as proposed by
\textcite{khan.2016.adaptive} and others~\cite{ziviani.2005.accuracy,
  hu.2012.millions, ding.2015.ip}.  We first measure RTTs to three
anchors per continent, and use these measurements to deduce which
continent the target is on.  We then randomly select and measure RTTs
to 25 more landmarks on that continent, from a list including all of
the anchors, plus all the probes that have been online for the past
30 days with a stable IPv4 address.  These probes are shown
in Figure~\ref{f:landmarks-map} (right side).

Random selection of landmarks in the second phase spreads out the load
of our measurements, reducing their impact on concurrent
experiments~\cite{holterbach.2015.ripe-interference}.  Using stable
probes as well as anchors spreads the load even in parts of the world
where there are few anchors.

We maintain a server that retrieves the list of anchors and probes
from RIPE's database every day, selects the probes to be used as
landmarks, and updates a delay-distance model for each landmark, based
on the most recent two weeks of ping measurements available from
RIPE's database.  Our measurement tools retrieve the set of landmarks
to use for each phase from this server, and report their measurements
back to it.  Some of the landmarks have both IPv4 and IPv6 addresses,
but the commercial proxy servers we are studying offer only IPv4
connectivity, so the server resolves the landmarks' hostnames itself
and sends only IPv4 addresses to the tools.

\subsection{Measurement tools}\label{s:meas-tool-web}

\begin{figure*}
  \centering
  \parbox[t]{2.1in}{\centering
  \includegraphics{figure/js-vs-c-overhead-lin}%
  \caption{Comparison of the CLI tool with the
    web application in two browsers, all running on Linux.}%
  \label{f:js-vs-c-overhead-lin}}
  \hspace{2em plus2em minus1.5em}%
  \parbox[t]{2.1in}{\centering
  \includegraphics{figure/js-vs-c-overhead-w10}%
  \caption{Comparison of four browsers running on Windows 10.}%
  \label{f:js-vs-c-overhead-w10}}
  \hspace{2em plus2em minus1.5em}%
  \parbox[t]{2.1in}{\centering
  \includegraphics{figure/js-vs-c-overhead-w10-out}%
  \caption{High outliers removed from
    Figure~\ref{f:js-vs-c-overhead-w10}.  The dashed line has the same
    slope in both figures.}%
  \label{f:js-vs-c-overhead-w10-out}}
\end{figure*}

\begin{figure}
  \centering
  \includegraphics{figure/js-vs-c-ping}
  \caption{Both measurement tools make TCP connections to port 80 on
    each landmark.  The CLI tool can use the low-level \texttt{connect}
    API; the web application must use the higher-level \texttt{fetch} API.
    We instruct \texttt{fetch} to send encrypted (HTTPS) traffic to
    the usual port for \emph{unencrypted} HTTP, forcing a protocol
    error.  The CLI tool reliably measures one round-trip time;
    the web application measures one or two round-trips, depending on
    whether the landmark is listening on port 80.}
  \label{f:js-vs-c-ping}
\end{figure}

Commercial proxy providers aggressively filter traffic through their
proxies.  Of the VPN servers we tested, roughly 90\% ignore ICMP ping
requests.  Similarly, 90\% of the default gateways for VPN tunnels
(i.e.\ the first-hop routers for the VPN servers) ignore ping requests
and do not send time-exceeded packets, which means we cannot see them
in a \texttt{traceroute} either.  Roughly a third of the servers
discard \emph{all} time-exceeded packets, so it is not possible to
\texttt{traceroute} through them at all.  Some servers even drop UDP
and TCP packets with unusual destination port numbers.

In short, the only type of network message we can reliably use to
measure round-trip time is a TCP connection on a commonly used port,
e.g.\ 80 (HTTP).  We implemented two measurement tools that use this
method to measure round-trip times to each landmark.

\paragraph{Command-line}
For measurements of VPN proxies' locations
(Section~\ref{s:proxy-locations}), we used a standalone program,
written in Python and C. It can take measurements either directly or
through a proxy, and it can process a list of proxies in one batch.

This tool uses the POSIX sockets API to make TCP connections.  It
measures the time for the \texttt{connect} primitive to succeed or
report “connection refused,” and then closes the connection without
sending or receiving any data.  We verified that \texttt{connect}
consistently returns as soon as the second packet of the TCP three-way
handshake arrives (i.e.\ after a single round-trip to a landmark) on
both Linux and NetBSD.  (Linux was used as the client OS for all the
measurements of VPN proxies; some pilot tests involved clients running
NetBSD.)  If a connection fails with an error code other than
“connection refused,” the measurement is discarded.  “Network
unreachable” errors, for instance, originate from intermediate
routers, so they do not reflect the full round-trip time.

\paragraph{Web-based}
For algorithm validation (Section~\ref{s:algorithm-testing}) we
crowdsourced hosts in known locations from around the world.  We could
not expect volunteers from the general public, or Mechanical Turk
workers, to download, compile, and run a command-line tool, so we
implemented a second measurement tool as a Web application.  Anyone
can access the website hosting this
application,\footnote{\url{https://research.owlfolio.org/active-geo}}
and it requires no “plug-ins” or software installation.  It presents a
live demonstration of active geolocation, displaying the measurements
as circles drawn on a map, much as in Figure~\ref{f:multilateration}.
After this demonstration is complete, it offers an explanation of the
process, and invites the user to upload the measurements to our
database, if they are willing to report their physical location.

The price of user-friendliness is technical limitations.  Web
applications are only allowed to send well-formed HTTP(S) messages; we
cannot close connections immediately upon establishment, without
sending or receiving any data, as the command-line tool does.

In principle, web applications are not allowed to communicate with
arbitrary hosts, only with the server hosting their
website~\cite{jackson2006protecting}.  However, this rule has a
loophole.  When a web application attempts to communicate with a
server that isn't hosting its website, the browser will send an HTTP
request, but won't return a successful response unless the server
allows it, using special HTTP response headers.  Errors are still
reported.  Since we only care about the time to connect, we make a
request that we know will fail, and measure the time to failure.
Ideally, we would connect to a TCP port that was not blacklisted by
any VPN provider, and was closed (not blackholed) on all the RIPE
Atlas nodes we use, but there is no such port.

Instead, the web application makes encrypted (HTTPS) connections to
the usual TCP port for \emph{unencrypted} HTTP~(80).  This will fail
after one round-trip if the landmark isn't listening on port 80.
However, if it is listening on port 80, the browser will reply to the
SYN-ACK with a TLS ClientHello packet.  This will trigger a protocol
error, and the browser will report failure, but only after a second
round-trip.  Thus, depending on whether the landmark is listening on
port 80 (which depends on the version of the RIPE Atlas node software
it is running; we cannot tell in advance) the web application will
measure the time for either one or two round-trips, and we can't tell
which.

\subsection{Tool Validation}\label{s:tool-validation}

Figure~\ref{f:js-vs-c-ping} shows the abstract difference in the
network traffic generated by the two tools.
Figure~\ref{f:js-vs-c-overhead-lin} compares the round-trip times
measured by the command-line tool and the web application running
under two different browsers, all three on a computer in a known
location running Linux, to a collection of landmarks as described in
Section~\ref{s:two-phase-measurement}.  We manually partitioned the
measurements taken by the web application into groups suspected to be
one round trip and two round trips, and estimated the distance-delay
relationship for each by linear regression, shown with black lines and
gray 95\% confidence intervals.  The slope of the two-round-trip line
is $1.96$ times the slope of the one-round-trip line; adjusted $R^2$
(considering both lines as a single model) is $0.9942$.  After
accounting for the effects of distance and whether we measured one or
two round trips, ANOVA finds no significant difference among the three
tools (two additional degrees of freedom, $F = 0.8262$, $p = 0.44$)
which is a testament to the efficiency of modern JavaScript
interpreters.

Figure~\ref{f:js-vs-c-overhead-w10} compares the round-trip times
measured by the web application running under four additional
browsers, on the same computer that was used for
Figure~\ref{f:js-vs-c-overhead-lin}, but running Windows 10 instead.
(The command-line tool has not been ported to Windows.)  Measurements
on Windows are much noisier than on Linux.  We can still distinguish a
group of one-round-trip data points and a group of two-round-trip data
points, but there is a third group, “high outliers,” separately shown
in Figure~\ref{f:js-vs-c-overhead-w10-out} so that
Figures~\ref{f:js-vs-c-overhead-lin} and~\ref{f:js-vs-c-overhead-w10}
can have the same vertical scale.  The diagonal dashed line on
Figures~\ref{f:js-vs-c-overhead-w10}
and~\ref{f:js-vs-c-overhead-w10-out} has the same absolute slope.  The
high outlier measurements are much slower than can be attributed to
even two round-trips, and their values are primarily dependent on the
browser they were measured with, rather than the distance.

%
% These figures belong with algo-testing.tex.
%
\begin{figure*}
  \centering
  \includegraphics{figure/crowdsourced-hosts}
  \caption[Locations of crowdsourced validators] {Locations of the
    crowdsourced hosts used for algorithm validation, with volunteers
    on the left and Mechanical Turk workers on the right.}
    \label{f:validation-map}
\end{figure*}

\begin{figure*}[t!]
\centering\includegraphics{figure/crowdsourced-calibration}
\caption{Precision of predicted regions for crowdsourced test hosts.}
\label{f:prediction-precision}
\end{figure*}


Excluding the high outliers, the remaining data points for Windows can
also be modeled by a division into groups for one or two round-trips,
but not as cleanly as on Linux.  The ratio of slopes is $2.29$,
adjusted $R^2 = 0.8983$, and ANOVA finds the model is significantly
improved by considering the browser as well (three more degrees of
freedom, $F = 13.11$, $p = 6.1 \times 10^{-8}$).  Equally concerning,
if we combine the two models, we find that the operating system has a
significant effect on the slopes of the lines (four additional degrees
of freedom, $F = 693.56$, $p < 2.2 \times 10^{-16}$) and the
regression line for two round-trips measured on Linux
($t = 0.03375d + 45.52$, distance in km, time in ms)
is about the same as the line for \emph{one} round-trip measured on
Windows ($t = 0.03288d + 49.92$).

In section~\ref{s:algorithm-testing}, we will speak further of how these
limitations affect our assessment of which algorithm is most suitable
for estimating the location of a proxy that could be anywhere in the
world.
