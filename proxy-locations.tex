\section{Locating VPN Proxies}\label{s:proxy-locations}

\begin{figure}[b!]
\centering\includegraphics{figure/vpn-location-provider-xtab}
\caption{The countries where 157 VPN providers claim to have proxies.
  Providers included in this study are colored and labeled.  Data
  provided by
  \href{https://www.vpn.com/}{VPN.com}~\cite{gargiulo.2018.vpncom}.}%
\label{f:location-provider-xtab}
\end{figure}

We used the two-phase, proxy-adapted CBG++ to test the locations of
proxies from seven VPN providers.  This paper's purpose is not to call
out any specific provider for false advertising, so we are not naming
the seven providers that we tested; however,
figure~\ref{f:location-provider-xtab} shows their rankings by
number of countries and dependencies claimed, with 150 of their
competitors for comparison.  Providers A through E are among the 20
that make the broadest claims, while F and G make more modest and
typical claims.  Notice that providers who claim only a few locations,
tend to claim more or less the same locations; this is what one would
expect if it were much easier to lease space in a data center in some
countries than others.

All of the VPN providers we tested use round-robin DNS for load
balancing; to avoid the possibility of unstable measurements, we
looked up all of the server hostnames in advance, from the same host
that would run the command-line measurement tool, and tested each IP
address separately.  We used a single client host for all of the
measurements, located in Frankfurt, Germany.  Because of this, we
cannot say whether the VPN providers might be using DNS geotargeting
or anycast routing to direct clients in different parts of the world
to different servers.  In total, we tested 2269 unique server IP
addresses, allegedly distributed over 222 countries and territories.

None of the providers advertise exact locations for their proxies.
At best they name a city, but often they only name a country.
City claims sometimes contradict themselves; for instance, we observed
a config file named “\texttt{usa.new-york-city.cfg}” directing the VPN
client to contact a server named
“\texttt{chicago.\discretionary{}{}{}vpn-\discretionary{}{}{}%
  pro\-vi\-der.\discretionary{}{}{}example}.”  Therefore, we only
evaluate country-level claims.

CBG++ tells us only that a proxy is within some region.  If that
region is big enough to cover more than one country, we can't be
certain where the server really is.  However, we might still be
certain that it \emph{isn't} where the proxy provider said it was; for
instance, a predicted region that covers Canada and the USA still
rules out the entire rest of the world.  We say that the provider's
claim for a proxy is \emph{false} if the predicted region does not
cover any part of the claimed country.  We say that it is
\emph{credible} if the predicted region is entirely within the claimed
country, and we say that it is \emph{uncertain} if the predicted
region covers both the claimed country and others.  For false and
uncertain claims, we also checked whether any of the countries covered
by the prediction region were on the same continent as the claimed
country.

\begin{figure}[t!]
  \centering
  \includegraphics[width=0.5\hsize]{figure/as-example/chile}
  \caption{Disambiguation by data center locations: the only data
    centers in this region are in Chile, not Argentina.}
  \label{f:disambig-dc}
\end{figure}

\begin{figure}[t!]
  \centering
  \includegraphics[width=0.66\hsize]{figure/as-example/AS63128}
  \caption{Disambiguation by metadata: all these hosts belong
    to the same provider, the same AS, and the same /24, so they
    are likely to be in the same physical location.}%
  \label{f:disambig-metadata}%
\end{figure}

\begin{figure*}[p!]
  \centering\includegraphics{figure/credible-alluvia-dc}
  \caption{Overall assessment of providers' claims to have proxies in
    specific countries.}\label{f:credible-alluvia}
  \centering\vspace{1em}\includegraphics{figure/vpn-location-provider-honesty}
  \caption{Credible claims are concentrated in the most commonly claimed
    countries.}\label{f:credible-ranking}
  \centering\vspace{1em}\includegraphics{figure/credible-countries}
  \caption{The credibility of each provider's claims for specific
    countries.}\label{f:credible-countries}
\end{figure*}


Some uncertain predictions can be resolved by referring to a list of
known locations of data centers, such as the one maintained by the
University of Wisconsin~\cite{impact.atlas.2011}.  For example, the
prediction shown in Figure~\ref{f:disambig-dc} is uncertain because
it covers Argentina as well as Chile.  However, the only data centers
within the region are in Chile, so we can conclude that this server is
in Chile.  When data center locations are not enough, cross-checking
with network metadata may help.  For example, in
Figure~\ref{f:disambig-metadata}, the largest of the 20 predicted
regions cover data centers on both sides of the USA-Canada border, but
all of the hosts share a provider, an autonomous system (AS), and a
24-bit network address, which means they are practically certain to be
in the \emph{same} data center.  Since all of the regions cover part
of Canada, but only some of them cross into the USA, we ascribe all of
these hosts to Canada.  Overall, these techniques allow us to
reclassify 353 uncertain predictions as credible or not-credible.

Putting it all together, we find that the claimed location is credible
for 989 of the 2269 IP addresses, uncertain for 642, and false for
638.  For 401 of the false addresses, the true location is not even on
the same continent as the claimed location; however, for 462 of the
uncertain addresses, the true location \emph{is} somewhere on the same
continent as the claimed location.  (See
Appendix~\ref{s:continents} for how we defined continental
boundaries, and some discussion of which countries and continents are
most likely to be confused.)

Figure~\ref{f:credible-alluvia} shows which countries, overall, are
more likely to host credibly-advertised proxies, and where the servers
for the false claims actually are.  The ten countries with the largest
number of claimed proxies account for 84\% of the credible cases, and
only 11\% of the false cases.  (Uncertain cases are nearly evenly
split between the top ten and the remainder.)  False claims are spread
over the “long tail” of countries, with only a few advertised servers
each.  Figure~\ref{f:credible-alluvia} also shows the overall
effect of using data center and AS information to disambiguate
predictions.  It is particularly effective when the prediction region
crosses continents; 55\% of those cases were completely resolved for
our purposes.  Only 23\% of the regions covering multiple countries
within the same continent could be disambiguated.

Figure~\ref{f:credible-ranking} shows another perspective on
the same observation, by relating credibility to the country ranking
in Figure~\ref{f:location-provider-xtab}.  The credible claims are
concentrated in the countries where many other VPN providers also
claim to host proxies.  This is evidence for our original intuition
that proxies are likely to be hosted in countries where server hosting
is easy to acquire.

We might also like to know if some providers are more reliable than
others.  Figure~\ref{f:credible-countries} shows, for each
provider, a map of the world with each country color-coded according
to the overall honesty of the provider's claims for that country.  If
a country is drawn in white, the provider didn't claim to have any
proxies in that country to begin with.  Bright green means all of the
claimed proxies' CBG++ predictions overlap the country at least
somewhat---that is, the “yes” or “uncertain” categories in
Figure~\ref{f:credible-alluvia}, after taking data center locations
into account.  Dark purple means none of the predictions overlap the
country at all.  Colors in between mean CBG++ backs up the claim for
some but not all of the proxies claimed to be in that country.

There is some variation among the providers; for instance, C and E are
actually hosting servers in more than one country of South America,
whereas providers A and B just say they are.  However, claimed
locations in countries where server hosting is difficult are almost
always false. Even in regions like Western Europe, where hosting
\emph{is} available in any country one would like, providers seem to
prefer to concentrate their hosts in a few locations.

\subsection{Data Centers and Prediction Error}\label{s:dc-prediction-error}

\begin{figure}
  \includegraphics{figure/as-example/as63128-nearest}
  \caption{For AS63128, the size of the prediction region is not
    correlated with the distance to the nearest landmark.}%
  \label{f:size-distance-no-corr}
\end{figure}

Groups of proxies that we believe are all in the same data center can
be used for another check on the accuracy of our geolocation methods.
We are not yet certain enough of our data center groups to run this
analysis on all of the grouped proxies, but we can discuss the results
for a clear-cut case like AS63128.  If geolocation worked perfectly,
all of the regions shown in Figure~\ref{f:disambig-metadata} ought
to be the same, but clearly they are not; there isn't even a single
sub-region that they all cover.  Since our two-phase procedure uses a
different randomly-selected group of landmarks for each measurement,
variation is to be expected.  Figure~\ref{f:size-distance-no-corr}
shows that there is no correlation between the size of each prediction
region, and the distance to the nearest landmark for that region from
the centroid of all the predictions taken together.  This means the
variation is not simply due to geographic distance; it may instead be
due to some landmarks experiencing more congestion or routing detours
than others.

\subsection{Comparison with ICLab and IP-to-Loc\-ation Databases}
\label{s:comparisons}

\begin{figure}
\centering
\includegraphics[width=0.95\hsize]{figure/vpn-vs-geo-heat}
\caption{The percentage of each provider's proxies for which
  our validation (two different ways), ICLab's validation, and five
  popular geolocation databases agree with the advertised location.}
\label{f:vpn-vs-geo-heat}
\end{figure}

Active geolocation has been applied to proxy servers once before, as
part of a larger project, ICLab, to automate monitoring for Internet
censorship across the entire
world~\cite{razaghpanah.2016.design-space}.  We contacted them and
they provided us with their data for comparison.

ICLab's geolocation checker only attempts to prove that each proxy is
\emph{not} in the claimed country.  It assumes that it is impossible
for a packet to have traveled faster than a configurable speed limit;
their actual tests used \kmms{153} (\SI{0.5104}{\clight}) for this
limit (slightly faster than the “speed of internet” described in
\textcite{katz.2006.delay}).  Given a country where a host is claimed
to be, and a set of round-trip measurements, ICLab's checker
calculates the minimum distance between each landmark and the claimed
country, then checks how fast a packet would have had to travel to
cover that distance in the observed time.  The claimed location is
only accepted if none of the packets had to travel faster than the
limit.

Figure~\ref{f:vpn-vs-geo-heat} shows the percentage of overall
claims by each proxy provider that our algorithm, ICLab's algorithm,
and five popular IP-to-location databases agree with.  The numbers for
CBG++ are calculated two ways: “generous” means we assume that all of
the “uncertain” cases are actually credible, and “strict” means we
assume they are all false.  ICLab's algorithm is even stricter than
ours, but most of that is explained by our more subtle handling of
uncertain cases.  Our “strict” numbers are usually within 10\% of
ICLab's.  Looking more deeply into the disagreements reveals that
CBG++ almost always predicts a location close to a national
border---just the situation where either algorithm could be tripped up
by an underestimate.

All five of the IP-to-location databases are more likely to agree with
the providers' claims than either active-geolocation approach is.  As
discussed earlier, we are inclined to suspect that this is because the
proxy providers have influenced the information in these databases.
We have no hard evidence backing this suspicion, but we observe that
there is no pattern to the countries for which the IP-to-location
databases disagree with provider claims.  This is what we would expect
to see if the databases were being influenced, but with some lag-time.
As the proxy providers add servers, the databases default their
locations to a guess based on IP address registry information, which,
for commercial data centers, may be reasonably close to the truth.
When the database services attempt to make a more precise assessment,
this draws on the source that the providers can influence.
