@default_files = ("paper.tex");
$dvi_mode = 0;
$pdf_mode = 1;
$bibtex_use = 2;
$clean_ext = "run.xml aaa ttt fff comment.cut";
$ENV{BIBINPUTS} = ".:bib:";
