\section{Introduction}\label{s:introduction}

Commercial VPN services compete to offer the highest speed, the
strongest privacy assurances, and the broadest possible set of server
locations.  One of the services in this study advertises servers in
all but seven of the world's sovereign states, including implausible
locations such as North Korea, Vatican City, and Pitcairn Island.
They offer no proof of their claims.  IP-to-location databases often
agree with their claims, but these databases have been shown to be
full of errors~\cite{shavitt.2011.databases, poese.2011.databases,
  gharaibeh.2017.router}.  Worse, they rely on information that VPN
providers may be able to manipulate, such as location codes in the
names of routers~\cite{chandra.2015.alidade}.

VPN services that consolidate their servers in a smaller number of
locations than they advertise can choose those locations for better
performance, reliability, and reduced operational expenses.  This
gives them a competitive advantage over services that strive for
true location diversity.  If they can manipulate IP-to-location
databases, they can still provide the \emph{appearance} of location
diversity.

Many of a VPN service's customers may well be satisfied with
appearances.  For instance, the IP-to-location database entry is more
important than the physical location for customers using VPNs to
defeat geographic restrictions on online media
streaming~\cite{abdou.2015.cpv}.  However, for others the physical
location can be essential.  We started the investigation leading to
this paper when we attempted to use commercial VPN services for
censorship monitoring, but could not reproduce the observations
reported by volunteers within a country known for censoring the
Internet.

In this paper, we apply \emph{active geolocation} to check the
advertised locations of VPN servers.  Active geolocation estimates the
location of an Internet host by measuring packet round-trip times
between it and other hosts in known locations.  It has been
demonstrated to work at the scale of a large country or small
continent (e.g.\ China, Europe, and the USA), with varying levels of
accuracy, depending on how efficient the regional network
is~\cite{eriksson.2013.geometry, li.2013.modconn, ding.2015.ip,
  chen.2016.landmark}.  However, it has not been thoroughly tested at
the scale of the entire world, and, to our knowledge, it has only once
before been applied to commercial proxy
servers~\cite{razaghpanah.2016.design-space}.

Using active geolocation, we can usually locate a VPN server to within
\SI{1000}{\km^2}, anywhere in the world.  Our results are more precise
in more densely connected regions and/or when landmarks are nearby,
but even when we are uncertain about where a server actually is, we
can still disprove blatant inaccuracies in marketing claims.  For
instance, if we know that a server is in Belgium, Netherlands, or
Germany, but not which, that still proves it is not in North Korea.
We tested 2269 servers operated by seven VPN services, including five
of the top 20 by number of claimed countries.  \emph{At least a third
  of all the servers we tested are not in their advertised country.}
